from torch import Tensor
import torch


def get_noised_images(
        images: Tensor, noise_factor: float
) -> Tensor:
    """
    Get noised images
    :param images: batch size, num channels, height, width
    :param noise_factor: noise factor
    :return: batch size, num channels, height, width
    """
    noised_images = images + noise_factor * torch.normal(
        mean=0.0, std=1.0, size=images.shape
    )
    noised_images = torch.clip(noised_images, min=0.0, max=1.0)
    return noised_images
