import streamlit as st
from typing import Dict
import yaml
import torch.nn as nn
import torch
from model import AutoEncoderModel
from torch.utils.data import Dataset
from torchvision.datasets import MNIST
from torchvision.transforms import ToTensor
import random
from utils import get_noised_images
from torchvision.utils import make_grid


st.set_page_config(
    page_title="MNIST auto denoise demo",
    layout="centered"
)


st.title("MNIST auto denoise demo")
st.markdown("---")


@st.cache_resource
def load_config() -> Dict:
    """
    Load config
    :return:
    """
    with open("config.yaml", mode="r") as file_obj:
        result: Dict = yaml.safe_load(file_obj)
    return result


config: Dict = load_config()
device = torch.device(config["device"])


@st.cache_resource
def load_model() -> nn.Module:
    """
    Load model
    :return:
    """
    result = AutoEncoderModel(
        in_channels=config["in_channels"],
        latent_channels=config["latent_channels"]
    ).to(device)
    result.load_state_dict(
        torch.load(
            f'{config["output_dir"]}/model.pt',
            map_location=device
        )
    )
    result.eval()
    return result


@st.cache_resource
def load_dataset() -> Dataset:
    """
    Load dataset
    :return:
    """
    result = MNIST(
        root=config["dataset_path"], download=True,
        transform=ToTensor()
    )
    return result


model = load_model()
dataset = load_dataset()
indexes = [
    random.randint(a=0, b=len(dataset))
    for _ in range(config["num_demo_images"])
]
images = torch.stack(
    [
        dataset[i][0] for i in indexes
    ], dim=0
)
noised_images = get_noised_images(
    images=images, noise_factor=config["noise_factor"]
)
with torch.no_grad():
    denoise_images = model(x=noised_images.to(device))
    denoise_images = torch.clip(denoise_images, min=0.0, max=1.0)
    denoise_images = denoise_images.cpu().detach()

images = make_grid(
    images, nrow=config["num_demo_images"], padding=1, pad_value=1.0
)
noised_images = make_grid(
    noised_images, nrow=config["num_demo_images"], padding=1, pad_value=1.0
)
denoise_images = make_grid(
    denoise_images, nrow=config["num_demo_images"], padding=1, pad_value=1.0
)

st.subheader("Origin image")
st.image(
    torch.permute(images, dims=(1, 2, 0)).numpy()
)
st.subheader("Noised image")
st.image(
    torch.permute(noised_images, dims=(1, 2, 0)).numpy()
)
st.subheader("Denoise image")
st.image(
    torch.permute(denoise_images, dims=(1, 2, 0)).numpy()
)

st.markdown("---")
st.button(label="Render other examples")

