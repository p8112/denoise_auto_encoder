import torch
from torchvision.datasets import MNIST
import yaml
from typing import Dict, List
from torchvision.transforms import ToTensor
from torch.utils.data import DataLoader
from utils import get_noised_images
from model import AutoEncoderModel
from torch.optim import Adam
from tqdm import tqdm
from torch.nn.functional import mse_loss
from pathlib import Path


with open("config.yaml", mode="r") as file_obj:
    config: Dict = yaml.safe_load(file_obj)


Path(config['output_dir']).mkdir(parents=True, exist_ok=True)


dataset = MNIST(
    root=config["dataset_path"], download=True,
    transform=ToTensor()
)
dataloader = DataLoader(
    dataset=dataset, num_workers=config["num_workers"],
    batch_size=config["batch_size"], shuffle=False
)


device = torch.device(config["device"])
model = AutoEncoderModel(
    in_channels=config["in_channels"],
    latent_channels=config["latent_channels"]
).to(device)
optimizer = Adam(
    params=model.parameters(), lr=config["learning_rate"]
)


def training_epoch(epoch: int) -> float:
    """
    Execute a training epoch
    :param epoch:
    :return: loss
    """
    model.train()

    step_losses: List = []
    progress_bar = tqdm(
        iterable=enumerate(dataloader),
        desc=f"Training epoch {epoch} ..."
    )
    for step, batch in progress_bar:
        images = batch[0]
        noised_images = get_noised_images(
            images=images, noise_factor=config["noise_factor"]
        )
        denoise_images = model(x=noised_images.to(device))
        loss = mse_loss(input=denoise_images, target=images.to(device))

        loss.backward()
        if (step + 1) % config["accumulate_gradient_batches"] == 0:
            optimizer.step()
            optimizer.zero_grad()

        loss_value: float = loss.item()
        step_losses.append(loss_value)
        if (step + 1) % config["update_bar_interval"] == 0:
            progress_bar.set_description(
                f"Training epoch {epoch}: loss {loss_value:.6f} ..."
            )
            progress_bar.update()
    progress_bar.close()

    return sum(step_losses) / len(step_losses)


for i in range(config["num_epochs"]):
    train_loss: float = training_epoch(epoch=i)
    print(f"Finish epoch {i}: loss: {train_loss:.6f}")
    torch.save(
        model.state_dict(),
        f'{config["output_dir"]}/model.pt'
    )
