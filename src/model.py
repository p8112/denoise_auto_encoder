import torch.nn as nn
from torch import Tensor


class Encoder(nn.Module):
    """
    Encoder model
    """
    def __init__(
            self, in_channels: int, out_channels: int
    ):
        """
        Init method
        :param in_channels: number channels of input image
        :param out_channels: number channels of output image
        """
        super(Encoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels, out_channels=out_channels,
                kernel_size=(3, 3), stride=(1, 1),
                padding=(1, 1), bias=True
            ),
            nn.LeakyReLU(),
            nn.MaxPool2d(
                kernel_size=(2, 2), stride=(2, 2)
            ),
            nn.Conv2d(
                in_channels=out_channels, out_channels=out_channels,
                kernel_size=(3, 3), stride=(1, 1),
                padding=(1, 1), bias=True
            ),
            nn.LeakyReLU(),
            nn.MaxPool2d(
                kernel_size=(2, 2), stride=(2, 2)
            )
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Compute encode
        :param x: batch size, in_channels, height, width
        :return: batch size, out_channels, height // 4, width // 4
        """
        return self.encoder(x)


class Decoder(nn.Module):
    """
    Decoder model
    """
    def __init__(
            self, in_channels: int, out_channels: int
    ):
        """
        Init method
        :param in_channels: number channels of input
        :param out_channels: number channels of output
        """
        super(Decoder, self).__init__()
        self.decoder = nn.Sequential(
            nn.Conv2d(
                in_channels=in_channels, out_channels=in_channels,
                kernel_size=(3, 3), stride=(1, 1),
                padding=(1, 1), bias=True
            ),
            nn.LeakyReLU(),
            nn.ConvTranspose2d(
                in_channels=in_channels, out_channels=in_channels,
                kernel_size=(4, 4), stride=(2, 2),
                padding=(1, 1), bias=False
            ),
            nn.ConvTranspose2d(
                in_channels=in_channels, out_channels=in_channels,
                kernel_size=(4, 4), stride=(2, 2),
                padding=(1, 1), bias=False
            ),
            nn.Conv2d(
                in_channels=in_channels, out_channels=out_channels,
                kernel_size=(3, 3), stride=(1, 1),
                padding=(1, 1), bias=True
            )
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Compute decode
        :param x: batch size, in channels, height, width
        :return: batch size, in channels, height * 4, width * 4
        """
        return self.decoder(x)


class AutoEncoderModel(nn.Module):
    """
    Auto encoder model
    """
    def __init__(
            self, in_channels: int, latent_channels: int
    ):
        """
        Init method
        :param in_channels: number channels of input
        :param latent_channels: latent channels
        """
        super(AutoEncoderModel, self).__init__()
        self.encoder = Encoder(
            in_channels=in_channels, out_channels=latent_channels
        )
        self.decoder = Decoder(
            in_channels=latent_channels, out_channels=in_channels
        )

    def forward(self, x: Tensor) -> Tensor:
        """
        Compute auto encode
        :param x: batch size, in channels, height, width
        :return: batch size, in channels, height, width
        """
        x = self.encoder(x=x)
        x = self.decoder(x=x)
        return x
